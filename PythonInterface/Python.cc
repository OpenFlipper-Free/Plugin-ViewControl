
/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#include <pybind11/pybind11.h>
#include <pybind11/embed.h>


#include <ViewControlPlugin.hh>
#include <OpenFlipper/BasePlugin/PythonFunctions.hh>
#include <OpenFlipper/PythonInterpreter/PythonTypeConversions.hh>

namespace py = pybind11;



PYBIND11_EMBEDDED_MODULE(ViewControl, m) {

  QObject* pluginPointer = getPluginPointer("ViewControl");

  if (!pluginPointer) {
     std::cerr << "Error Getting plugin pointer for Plugin-ViewControl" << std::endl;
     return;
   }

  ViewControlPlugin* plugin = qobject_cast<ViewControlPlugin*>(pluginPointer);

  if (!plugin) {
    std::cerr << "Error converting plugin pointer for Plugin-ViewControl" << std::endl;
    return;
  }

  // Export our core. Make sure that the c++ worlds core object is not deleted if
  // the python side gets deleted!!
  py::class_< ViewControlPlugin,std::unique_ptr<ViewControlPlugin, py::nodelete> > view(m, "ViewControl");

  // On the c++ side we will just return the existing core instance
  // and prevent the system to recreate a new core as we need
  // to work on the existing one.
  view.def(py::init([plugin]() { return plugin; }));



  view.def("selectionVisible",  &ViewControlPlugin::selectionVisible,
                                    QCoreApplication::translate("PythonDocViewControl","For meshes returns if the selection for this object is visible").toLatin1().data(),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","ID of the mesh object").toLatin1().data()) );

  view.def("showSelection",  &ViewControlPlugin::showSelection,
                                     QCoreApplication::translate("PythonDocViewControl","For meshes show or hide the selection.").toLatin1().data(),
                             py::arg(QCoreApplication::translate("PythonDocViewControl","ID of the mesh object").toLatin1().data()),
                             py::arg(QCoreApplication::translate("PythonDocViewControl","Visible or not?").toLatin1().data()) );

  view.def("modelingAreasVisible",  &ViewControlPlugin::modelingAreasVisible,
                                    QCoreApplication::translate("PythonDocViewControl","For meshes returns if the modeling area for this object is visible").toLatin1().data(),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","ID of the mesh object").toLatin1().data()) );

  view.def("showModelingAreas",  &ViewControlPlugin::showModelingAreas,
                                     QCoreApplication::translate("PythonDocViewControl","For meshes show or hide the modeling area.").toLatin1().data(),
                             py::arg(QCoreApplication::translate("PythonDocViewControl","ID of the mesh object").toLatin1().data()),
                             py::arg(QCoreApplication::translate("PythonDocViewControl","Visible or not?").toLatin1().data()) );

  view.def("setSelectionLineWidth",  &ViewControlPlugin::setSelectionLineWidth,
                                     QCoreApplication::translate("PythonDocViewControl","For meshes set the line width of selections.").toLatin1().data(),
                             py::arg(QCoreApplication::translate("PythonDocViewControl","ID of the mesh object").toLatin1().data()),
                             py::arg(QCoreApplication::translate("PythonDocViewControl","Line width to set").toLatin1().data()) );

  view.def("setFeatureLineWidth",  &ViewControlPlugin::setFeatureLineWidth,
                                     QCoreApplication::translate("PythonDocViewControl","For meshes set the line width of features.").toLatin1().data(),
                             py::arg(QCoreApplication::translate("PythonDocViewControl","ID of the mesh object").toLatin1().data()),
                             py::arg(QCoreApplication::translate("PythonDocViewControl","Line width to set").toLatin1().data()) );


//     /** Disable the given shader for a given object and draw mode.
//      * @param _objectId ID of the object for which to disable the shader
//      * @param _drawMode _drawMode for which the shader is disabled
//      * @param _shader Shader which is disabled. If this shader is not currently active for
//      *                the given _drawMode, nothing will be disabled. In order to disable
//      *                all shaders for the given _drawMode, use _shader=0 (default).
//      */
//     void disableShader(int _objectId, ACG::SceneGraph::DrawModes::DrawMode _drawMode, ShaderInfo* _shader=0);
//
//     /** Sets a shader for the object and the given drawMode( can be a combination of draw modes )
//      *  @param _id                 Object Id
//      *  @param _drawMode           ; separated list of drawmodes used by the shader
//      *  @param _shader             Shader information
//      */
//     void setShader(int _id, ACG::SceneGraph::DrawModes::DrawMode _drawMode, ShaderInfo _shader);

//     /** Sets a Shader for a specific draw mode
//      *
//      * @param _id        Object id that should use the shader
//      * @param _drawMode  Draw mode where this shader will be active
//      * @param _name      Name of the shader that should be used
//      */
//     void setShader(int _id, QString _drawMode, QString _name );
//
//     /// get information about available uniforms for a given shader
//     QStringList getUniforms(QString _shader);
//
//     QString getUniformType(QString _shader, QString _uniform );
//     QString getUniformDefault(QString _shader, QString _uniform );
//     QString getUniformMin(QString _shader, QString _uniform );
//     QString getUniformMax(QString _shader, QString _uniform );
//
//     /// set the value of a uniform in a shader for a specific drawMode
//     void setUniform(int _objID, ACG::SceneGraph::DrawModes::DrawMode _drawMode, QString _shader, QString _uniform, QString _value );

  view.def("setViewingDirection",  &ViewControlPlugin::setViewingDirection,
                                     QCoreApplication::translate("PythonDocViewControl","Set the viewing direction").toLatin1().data(),
                             py::arg(QCoreApplication::translate("PythonDocViewControl","Viewing Direction").toLatin1().data()),
                             py::arg(QCoreApplication::translate("PythonDocViewControl","Up vector").toLatin1().data()),
                             py::arg(QCoreApplication::translate("PythonDocViewControl","Viewer id to set viewing direction (Default is all viewers)?").toLatin1().data()) = PluginFunctions::ALL_VIEWERS );

  view.def("setSceneRadius",  &ViewControlPlugin::setSceneRadius,
                                    QCoreApplication::translate("PythonDocViewControl","Set scene radius").toLatin1().data(),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Radius").toLatin1().data()),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Viewer id to set radius (Default is all viewers)?").toLatin1().data()) = PluginFunctions::ALL_VIEWERS );

  view.def("rotate",  &ViewControlPlugin::rotate,
                                    QCoreApplication::translate("PythonDocViewControl","Rotate scene").toLatin1().data(),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Rotation axis").toLatin1().data()),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Rotation angle").toLatin1().data()),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Rotation center").toLatin1().data()),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Viewer id to rotate (Default is all viewers)?").toLatin1().data()) = PluginFunctions::ALL_VIEWERS );


  view.def("translate",  &ViewControlPlugin::translate,
                                    QCoreApplication::translate("PythonDocViewControl","Translate scene").toLatin1().data(),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Translation vector").toLatin1().data()),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Viewer id to translate (Default is all viewers)?").toLatin1().data()) = PluginFunctions::ALL_VIEWERS );

  view.def("setDrawMode",  &ViewControlPlugin::setDrawMode,
                                    QCoreApplication::translate("PythonDocViewControl","Set the draw mode of a viewer").toLatin1().data(),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","List of draw modes ( ; separated list ) ").toLatin1().data()),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Viewer id to set draw mode (Default is the active viewer)?").toLatin1().data()) = PluginFunctions::ACTIVE_VIEWER );

  view.def("setObjectDrawMode",  &ViewControlPlugin::setObjectDrawMode,
                                    QCoreApplication::translate("PythonDocViewControl"," This function can be used to set the drawmode for an object.").toLatin1().data(),
                              py::arg(QCoreApplication::translate("PythonDocViewControl","List of draw modes ( ; separated list ) ").toLatin1().data()),
                              py::arg(QCoreApplication::translate("PythonDocViewControl","Object ID to set the drawmode on.").toLatin1().data()),
                              py::arg(QCoreApplication::translate("PythonDocViewControl","Set the draw mode even if its not directly supported by the objects nodes").toLatin1().data()) = true);

  view.def("viewingDirection",  &ViewControlPlugin::viewingDirection,
                                    QCoreApplication::translate("PythonDocViewControl","Get the current viewing direction").toLatin1().data(),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Viewer id to get viewing direction from (Default is the active viewer)?").toLatin1().data()) = PluginFunctions::ACTIVE_VIEWER );

  view.def("upVector",  &ViewControlPlugin::upVector,
                                    QCoreApplication::translate("PythonDocViewControl","Get the current upVector").toLatin1().data(),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Viewer id to get upVector from (Default is the active viewer)?").toLatin1().data()) = PluginFunctions::ACTIVE_VIEWER );

  view.def("eyePosition",  &ViewControlPlugin::eyePosition,
                                    QCoreApplication::translate("PythonDocViewControl","Get the current eyePosition").toLatin1().data(),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Viewer id to get eyePosition from (Default is the active viewer)?").toLatin1().data()) = PluginFunctions::ACTIVE_VIEWER );

  view.def("sceneCenter",  &ViewControlPlugin::sceneCenter,
                                    QCoreApplication::translate("PythonDocViewControl","Get the current sceneCenter").toLatin1().data(),
                            py::arg(QCoreApplication::translate("PythonDocViewControl","Viewer id to get scene center from (Default is the active viewer)?").toLatin1().data()) = PluginFunctions::ACTIVE_VIEWER );

  view.def("setSceneCenter",  &ViewControlPlugin::setSceneCenter,
                                   QCoreApplication::translate("PythonDocViewControl","Set the scene center").toLatin1().data(),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","Coordinates of center?").toLatin1().data()),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","Viewer id to change (Default is all viewers)?").toLatin1().data()) = PluginFunctions::ALL_VIEWERS );

  view.def("enableBackfaceCulling",  &ViewControlPlugin::enableBackfaceCulling,
                                   QCoreApplication::translate("PythonDocViewControl","Enable or disable Backface culling").toLatin1().data(),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","Enable?").toLatin1().data()),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","Viewer id to change (Default is all viewers)?").toLatin1().data()) = PluginFunctions::ALL_VIEWERS );

  view.def("setEyePosition",   &ViewControlPlugin::setEyePosition),
                                   QCoreApplication::translate("PythonDocViewControl","Change the Eye position to the given value").toLatin1().data(),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","Eye position").toLatin1().data()) ;

  view.def("viewAll",   static_cast<void (ViewControlPlugin::*)()>(&ViewControlPlugin::viewAll),
                                   QCoreApplication::translate("PythonDocViewControl","Change View in all viewers to view whole scene").toLatin1().data());
  view.def("viewAll",   static_cast<void (ViewControlPlugin::*)(int)>(&ViewControlPlugin::viewAll),
                                   QCoreApplication::translate("PythonDocViewControl","Change View in given viewer to view whole scene").toLatin1().data(),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","Id of the viewer which should be switched").toLatin1().data()) );

  view.def("viewHome",   static_cast<void (ViewControlPlugin::*)()>(&ViewControlPlugin::viewHome),
                                   QCoreApplication::translate("PythonDocViewControl","Change View on all viewers to view home position").toLatin1().data());
  view.def("viewHome",   static_cast<void (ViewControlPlugin::*)(int)>(&ViewControlPlugin::viewHome),
                                   QCoreApplication::translate("PythonDocViewControl","Change View on given viewer to view home position").toLatin1().data(),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","Id of the viewer to change").toLatin1().data()) );

  view.def("orthographicProjection",   static_cast<void (ViewControlPlugin::*)()>(&ViewControlPlugin::orthographicProjection),
                                   QCoreApplication::translate("PythonDocViewControl","Change all Viewers to orthographic projection").toLatin1().data());
  view.def("orthographicProjection",   static_cast<void (ViewControlPlugin::*)(int)>(&ViewControlPlugin::orthographicProjection),
                                   QCoreApplication::translate("PythonDocViewControl","Change Viewer to orthographic projection").toLatin1().data(),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","Id of the viewer to change").toLatin1().data()) );

  view.def("perspectiveProjection",   static_cast<void (ViewControlPlugin::*)()>(&ViewControlPlugin::orthographicProjection),
                                   QCoreApplication::translate("PythonDocViewControl","Change all Viewers to perspective projection").toLatin1().data());
  view.def("perspectiveProjection",   static_cast<void (ViewControlPlugin::*)(int)>(&ViewControlPlugin::orthographicProjection),
                                   QCoreApplication::translate("PythonDocViewControl","Change Viewer to perspective projection").toLatin1().data(),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","Id of the viewer to change").toLatin1().data()) );

  view.def("setFOVY",  &ViewControlPlugin::setFOVY,
                                   QCoreApplication::translate("PythonDocViewControl","Set fovy angle of projection for all viewers.").toLatin1().data(),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","fovy angle").toLatin1().data()) );

  view.def("setCoordsysProjection",  &ViewControlPlugin::setCoordsysProjection,
                                   QCoreApplication::translate("PythonDocViewControl","Set the projection mode of the coordinate system.").toLatin1().data(),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","If true, orthogonal projection otherwise perspective projection").toLatin1().data()) );

  view.def("setTwoSidedLighting",  &ViewControlPlugin::setTwoSidedLighting,
                                   QCoreApplication::translate("PythonDocViewControl","Enable or disable two sided lighting.").toLatin1().data(),
                           py::arg(QCoreApplication::translate("PythonDocViewControl","Specifies whether to enable or disable two sided lighting.").toLatin1().data()) );

  view.def("project", &ViewControlPlugin::project,
                                   QCoreApplication::translate("PythonDocViewControl","Use the projection matrix of the given viewer to project the point").toLatin1().data(),
                                   py::arg(QCoreApplication::translate("PythonDocViewControl","Coordinates of a point").toLatin1().data()),
                                   py::arg(QCoreApplication::translate("PythonDocViewControl","ViewerId").toLatin1().data()) = 0 );

//
//   emit setSlotDescription("setObjectDrawMode(QString,int,bool)", "Set the drawMode for an object",
//                           QString("DrawMode,ObjectID,Force").split(","),
//                           QString("the drawMode ( ; separated list ),Object id,Apply without checking support(default is true)").split(","));


}

